(define-module (orange packages linux-gallium)
  #:use-module (gnu packages)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages linux)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system linux-module)
  #:use-module (guix build-system trivial)
  #:use-module ((guix licenses) #:prefix license:))

(define (cursed-linux freedo version hash)
  (package
    (inherit freedo)
    (name "linux")
    (version version)
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/GalliumOS/linux.git")
                    (commit "f34a44068b42215c0e60c1068e498ea64895b5eb")))
              (file-name (git-file-name name version))
              (sha256
               (base32 hash))))
    (build-system trivial-build-system)
    (arguments
     `(#:configuration-file (string-append (assoc-ref %outputs "out") "/galliumos/config")))
    (home-page "https://www.kernel.org/")
    (synopsis "Linux kernel for GalliumOS")
    (description "same")
    (license license:gpl2)))

(define-public linux-gallium
  (cursed-linux linux-libre-4.15 "4.15.18"
                "12vf4bzdbhkc6s69yxdrky44yn9x12x75cgpdh069pwj3rj3idip"))
